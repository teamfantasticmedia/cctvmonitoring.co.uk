<?php
/*
** page.php
** mk_build_main_wrapper : builds the main divisions that contains the content. Located in framework/helpers/global.php
** mk_get_view gets the parts of the pages, modules and components. Function located in framework/helpers/global.php
*/

get_header();


Mk_Static_Files::addAssets('mk_button');
Mk_Static_Files::addAssets('mk_audio');
Mk_Static_Files::addAssets('mk_swipe_slideshow');

$banner_text = get_field('heading_text');
$banner_image = get_field('acf_banner_image');
if (null !== $banner_text || null !== $banner_image) { ?>
    <style type="text/css">
        .single__banner h1 {
            background-image: url('<?php echo get_field('heading_logo'); ?>');
        }
    </style>
    <div class="single__banner single__banner--aisite <?php echo get_field('show_banner_cta') ? 'single__banner--has-cta' : ''; ?>" style="background-image: url('<?php echo $banner_image; ?>');">
        <div class="single__banner--inner">
            <?php if (get_field('heading_text')) {
                    the_field('heading_text');
                } ?>
        </div>
    </div>
<?php }

$show_cta = get_field('show_banner_cta');
if ($show_cta) { ?>
    <div class="banner__cta banner__cta--<?php the_field('banner_cta_colour_scheme'); ?>">
        <?php if (get_field('banner_cta_title')) { ?>
            <h3><?php the_field('banner_cta_title'); ?></h3>
        <?php } ?>
        <?php if (get_field('banner_cta_text')) { ?>
            <?php the_field('banner_cta_text'); ?>
        <?php } ?>
    </div>
<?php }



mk_build_main_wrapper(mk_get_view('singular', 'wp-page', true));


get_footer();
