<?php
$icons = array(
	array(
		"icon" => "step-1.svg",
		"title" => "Intruder Enters Site",
		"copy" => "High definition CCTV cameras monitor your site 24/7, looking out for any suspicious activity."
	),
	array(
		"icon" => "step-2.svg",
		"title" => "Detection Triggers / Alarms",
		"copy" => "Passive infrared detector is activated as intruder passes. Detector triggers an alarm to be sent to control room with a real time video stream of related camera footage."
	),
	array(
		"icon" => "step-3.svg",
		"title" => "Control Room Analyse Threat",
		"copy" => "Operator opens alarm and reviews footage."
	),
	array(
		"icon" => "step-4.svg",
		"title" => "Audio Warning Issued",
		"copy" => "If a threat is detected an immediate live audio warning is issued, this will usually result in the intruder leaving site."
	),
	array(
		"icon" => "step-5.svg",
		"title" => "Intruder Tracked On Screen",
		"copy" => "Monitoring operative tracks intruder and calls for response from either the police or our uniformed partners."
	),
	array(
		"icon" => "step-6.svg",
		"title" => "Security Respond To The Threat",
		"copy" => "Uniformed response detain/check for intruder and ensure site is secured again before leaving."
	)
);

/**
 * Rotates a point around a pivot
 * @param float $x Pivot point X to rotate around
 * @param float $y Pivot point Y to rotate around
 * @param int $angle The angle to rotate it by
 * @param array $p[x,y] The point to translate
 * @return array The translated point
 */
function rotate($x, $y, $angle, $p)
{

	$angle = deg2rad($angle);

	$s = sin($angle);
	$c = cos($angle);

	$p['x'] -= $x;
	$p['y'] -= $y;

	$xN = $p['x'] * $c - $p['y'] * $s;
	$yN = $p['x'] * $s + $p['y'] * $c;

	$p['x'] = $xN + $x;
	$p['y'] = $yN + $y;

	return $p;
}
?>

<div class="six-step-process">

	<div class="six-step-process__column">

		<div class="six-step-process__icons">

			<div class="six-step-process__icons-inr ssp-icons">

				<div class="ssp-icons__main">
					<img src="<?php echo get_stylesheet_directory_uri() ?>/fm-custom-strips/six-step-process/step-1.svg" />
				</div>

				<?php foreach ($icons as $i => $icon) : ?>
					<?php $points = rotate(50, 50, ($i + 1) * (360 / count($icons)), array('x' => 10, 'y' => 0)); ?>
					<a href="#<?php echo preg_replace("~[^a-z0-9\-_]+~", "", str_replace(" ", "-", strtolower($icon['title']))) ?>" class="ssp-icons__icon<?php if ($i == 0) echo " _active" ?>" style="left: <?php echo $points["x"] ?>%; top: <?php echo $points["y"] ?>%;">
						<img src="<?php echo get_stylesheet_directory_uri() ?>/fm-custom-strips/six-step-process/<?php echo $icon['icon'] ?>" />
					</a>
				<?php endforeach; ?>

			</div>

		</div>

	</div>

	<div class="six-step-process__column six-step-process__column--copy">

		<h2>6 Step Process</h2>

		<?php foreach ($icons as $i => $icon) : ?>

			<div class="six-step-process__copy<?php if ($i == 0) echo " _active" ?>" id="<?php echo preg_replace("~[^a-z0-9\-_]+~", "", str_replace(" ", "-", strtolower($icon['title']))) ?>">

				<h3><?php echo $icon['title'] ?></h3>

				<p><?php echo $icon['copy'] ?></p>

			</div>

		<?php endforeach; ?>

	</div>

</div>

<style type="text/css">
	.six-step-process {
		padding: 30px 0;
	}

	.six-step-process img {
		width: 100%;
		height: auto;
	}

	.six-step-process h2 {
		color: #1dbef8;
		font-size: 18px;
	}

	.six-step-process h3 {
		font-size: 30px;
	}

	.six-step-process h3,
	.six-step-process p {
		color: white;
	}

	.six-step-process:after {
		display: table;
		width: 100%;
		content: ' ';
		clear: both;
	}

	.six-step-process__column--copy {
		padding: 15px;
	}

	.six-step-process__icons {
		width: 100%;
		max-width: 300px;
		margin: 0 auto;
	}

	.six-step-process__icons-inr {
		position: relative;
		padding-top: 100%;
	}

	.ssp-icons__main {
		width: 49.3333%;
		position: absolute;
		left: 25.3333%;
		top: 25.3333%;
	}

	.ssp-icons__icon {
		width: 15.3333%;
		display: block;
		position: absolute;
		margin: -7.66665% 0 0 -7.66665%;
		opacity: 0.4;
		transition: opacity 0.2s;
	}

	.ssp-icons__icon:hover,
	.ssp-icons__icon._active {
		opacity: 1;
	}

	.six-step-process__copy {
		display: none;
		min-height: 220px;
	}

	.six-step-process__copy._active {
		display: block;
	}

	@media (min-width: 768px) {
		.six-step-process {
			padding: 80px 0;
		}

		.six-step-process__column {
			float: left;
			width: 50%;
		}
	}
</style>

<script type="text/javascript">
	(function($) {
		$(".ssp-icons__icon").click(function(e) {
			e.preventDefault();
			// Swap active states on the icons
			$(".ssp-icons__icon._active").removeClass("_active");
			$(this).addClass("_active");
			// Switch the main icon
			$(".ssp-icons__main > img").attr("src", $(this).children("img").attr("src"));
			// Swap copy
			$(".six-step-process__copy._active").removeClass("_active");
			$($(this).attr("href")).addClass("_active");
		});
	})(jQuery);
</script>